'use strict';

const FS = require('fs');
const Robot = require('./lib/robot');
const LostPositions = require('./lib/lost-positions');

// Get the input from the sample sinput files
var stream = FS.createReadStream('sample/input.txt');
var input = '';
stream.on('data', (data) => {
    input += data.toString();
}).on('end', () => {
    processInput(input.trim().split("\n"));
}).on('error', (error) => {
    console.log('Problem in opening the input file');
});

// Extract data from the input to create grid, robots and perform actions
var processInput = (data) => {
    var grid = data[0].split(' ');
    grid = {
        x: grid[0],
        y: grid[1]
    }
    data = data.splice(1);

    if (data.length % 2 != 0) {
        console.log('Not enough data'); // Each robot should have 2 lines of input
    } else {
        var lostPositions = new LostPositions();

        // Process robot data
        try {
            for(let i = 0; i < data.length; i += 2) {
                var placement = data[i].split(' ');
                var actions = data[i+1].split('');
                var robot = new Robot(grid, ...placement, lostPositions);
                var result = robot.performActions(actions);
                console.log(result.join(' '));
            }
        } catch (e) {
            console.log(e.message);
        }
    }
};
