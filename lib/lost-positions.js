'use strict';

/**
 *  A class to keep track of positions where robots were lost
 */
class LostPositions {
    constructor() {
        this.lostPositions = [];
    }

    addPosition(placementX, placementY, orientation) {
        this.lostPositions.push(placementX + '-' + placementY + '-' + orientation);
    }

    getPositions() {
        return this.lostPositions.map((item) => {
            item = item.split('-');
            var lost = {
                placementX: item[0],
                placementY: item[1],
                orientation: item[2]
            };
            return lost;
        })
    }

    isLost (placementX, placementY, orientation) {
        var item = placementX + '-' + placementY + '-' + orientation;
        return (this.lostPositions.indexOf(item) >= 0);
    }
}

module.exports = LostPositions;
