'use strict';

/**
 *  A class for robot creation and making them perform actions
 */
class Robot {
    constructor (grid, placementX, placementY, orientation, lostPositions) {
        this.grid = grid;
        this.placementX = parseInt(placementX);
        this.placementY = parseInt(placementY);
        this.orientation = orientation.toUpperCase();
        this.lostPositions = lostPositions;
        this.isLost = false;

        if (this.placementX > this.grid.x || this.placementY > this.grid.y) {
            throw new Error('Robot cannot be placed outside the grid');
        }
    }

    performActions(actions) {
        for(let i = 0; i < actions.length; i++) {
            var action = actions[i].toUpperCase();
            var prevPosition = [this.placementX , this.placementY, this.orientation];
            switch (action) {
                case 'F':
                    this.move();
                    break;
                case 'R':
                case 'L':
                    this.turn(action);
                    break;
            }

            if (this.isLost) {
                this.lostPositions.addPosition(...prevPosition);
                break;
            }
        }

        if (this.isLost) {
            return [...prevPosition, 'LOST'];
        } else {
            return [this.placementX , this.placementY, this.orientation];
        }
    }

    turn (action) {
        var directions = ['N', 'E', 'S', 'W'];
        var direction = directions.indexOf(this.orientation);
        switch (action) {
            case 'R':
                direction = (direction + 1 < directions.length) ? direction + 1 : 0;
                break;
            case 'L':
                direction = (direction - 1 < 0) ? directions.length - 1 : direction - 1;
                break;
        }
        this.orientation = directions[direction];
    }

    move () {
        if (!this.lostPositions.isLost(this.placementX, this.placementY, this.orientation)) {
            switch (this.orientation) {
                case 'N':
                    this.placementY++;
                    break;
                case 'E':
                    this.placementX++;
                    break;
                case 'S':
                    this.placementY--;
                    break;
                case 'W':
                    this.placementX--;
                    break;
            }
        }

        this.isLost = (this.grid.x < this.placementX || this.grid.y < this.placementY);
    }
}

module.exports = Robot;
