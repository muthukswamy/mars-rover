# Mars Rover
A program to process robot movements in a martian surface. Takes the input from an input file which size of the surface, robots initial position and the actions to perform. Output is the final position of the robot and if the robot is gone outside the martian surface. Details of the problem can be found here: [Problem details](Details.md)

## Requirements
NodeJS 8.9+

## Usage
To run the program, clone the repository locally and run the following command in the terminal:
```
node index.js
```
To update the input, edit the sample [input file](sample/input.txt)
